
const gulp = require("gulp"),
    concat = require("gulp-concat"),
    clean = require("gulp-clean"),
    browserSync = require("browser-sync").create();

const PATH = {
    src: {
        css: "./src/css/*.css",
        js: "./src/js/*.js",
    },
    build: {
        css: "./build/css/",
        js: "./build/js/",
        self: "./build",
    },
};

const cssBuild = () =>
    gulp
        .src("./src/css/*.css")
        .pipe(concat("style.css"))
        .pipe(gulp.dest("./build/css/"));

const jsBuild = () =>
    gulp
        .src("./src/js/*.js")
        .pipe(concat("script.js"))
        .pipe(gulp.dest("./build/js/"));

const cleanBuild = () => gulp.src("./build").pipe(clean());
const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
    });
    gulp.watch(PATH.src.css, cssBuild).on("change", browserSync.reload);
    gulp.watch(PATH.src.js, jsBuild).on("change", browserSync.reload);
};

gulp.task("cssBuild", cssBuild);
gulp.task("jsBuild", jsBuild);
gulp.task("cleanBuild", cleanBuild);
gulp.task("watcher", watcher);

gulp.task("default", gulp.series(cleanBuild, cssBuild, jsBuild, watcher));

